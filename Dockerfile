# Use Node.js base image
FROM node:16.15.0

# Set working directory
WORKDIR /app


# Install Python and pip
RUN apt-get update && \
    apt-get install -y python3 python3-pip && \
    ln -sf /usr/bin/python3 /usr/bin/python && \
    ln -sf /usr/bin/pip3 /usr/bin/pip

RUN npm install -g browserify
# Copy package.json and package-lock.json
RUN npm install -g npm@9.8.1

COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the files
COPY . .
RUN npm run browserify 
EXPOSE 3003

# Command to start the application
CMD ["npm", "run","dev"]